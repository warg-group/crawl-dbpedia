import json
from time import sleep

import sys
import pandas as pd
from SPARQLWrapper import SPARQLWrapper, JSON


# Creating a function to query properties of a class
def the_query(classe):
    sparqlwd = SPARQLWrapper("http://dbpedia.org/sparql")
    sparqlwd.setQuery('''
    PREFIX dbo: <http://dbpedia.org/ontology/>

    SELECT (GROUP_CONCAT(DISTINCT ?predicate; separator=",") AS ?itemset)
    WHERE {
      <http://dbpedia.org/resource/%s> ?predicate ?object .
      FILTER(isURI(?object) || isLiteral(?object))
      FILTER(contains(str(?predicate), str(dbo:)))
      FILTER(!contains(str(?predicate), "wikiPage"))
    }
    ''' % classe)
    sparqlwd.setReturnFormat(JSON)
    results = sparqlwd.query().convert()
    return pd.json_normalize(results)["results.bindings"][0]

# Creating a function to wrap the_query and handle errors
def get_entities_rules(page):
    waou = None
    while waou is None:
        try:
            waou = the_query(classe=page)
        except:
            print("error with entity %s" % page)
            sleep(2)
    try:
        return waou[0]['itemset']['value'].replace('http://dbpedia.org/ontology/', '').split(",")
    except:
        return []


if len(sys.argv) < 2:
    print("No argument")
    exit(1)

# Loop to get properties of all entities
print(sys.argv[1])
to_save = []
with open("./%s" % sys.argv[1], 'r') as f:
    for n, j in enumerate(f.readlines()):
        j = bytes(j[:-1], 'ascii').decode('unicode-escape')
        to_save.append(get_entities_rules(j))
        sleep(0.09)
        if (n + 1) % 10 == 0:
            sleep(0.01)
        if (n + 1) % 100 == 0:
            print(n)
            sleep(0.1)

# Saving the result to a json file
with open("./out/%s.json" % sys.argv[1], 'w') as f:
    json.dump(to_save, f)

from yaml import dump
import os

# Get all organisation files
ll = []
for path in os.scandir("./"):
    if path.is_file() and path.name.startswith("x"):
        ll.append(path.name)

ll.sort()

# Creating the main CI dictionnary
dico = {"stages": ["build", "gather", "gather_all"],
        "default": {"image": "python:3.7"},
        "gather_all": {"stage": "gather_all", "script": ["ls"], "needs": [], "artifacts": {"paths": ["$CI_PROJECT_DIR/out/*"]}}}

gather_jobs = []
for i, f in enumerate(ll):
    if i % 50 == 0:
        gather_jobs.append({"stage": "gather", "script": ["ls"], "needs": [], "artifacts": {"paths": ["$CI_PROJECT_DIR/out/*"]}})
    gather_jobs[-1]["needs"].append("%s_build" % f)

for i, f in enumerate(gather_jobs):
    dico["gather_job_%d" % i] = f
    dico["gather_all"]["needs"].append("gather_job_%d" % i)

# Creating the crawling jobs
for f in ll:
    dico["%s_build" % f] = {"timeout": "5 hours",
                            "stage": "build",
                            "script": ["pip install -r requirements.txt",
                                       "python3 download_file.py %s" % f],
                            "artifacts": {"paths": ["$CI_PROJECT_DIR/out/*"]}}

with open(".gitlab-ci.yml", 'w') as f:
    dump(dico, f)

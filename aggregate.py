import json
import os

# Get all result files
ll = []
for path in os.scandir("./out"):
    if path.is_file() and path.name.startswith("x"):
        ll.append(path.name)

ll.sort()

# Concatenate all files into one
sume = []

for f in ll:
    with open("./out/%s" % f, 'r') as f:
        tmp = json.load(f)
        sume += tmp

# Save the result
with open("./out/final.json", 'w') as f:
    json.dump(sume, f)
